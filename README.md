# Teste Invillia

Este projeto provê uma API RESTful que atende aos requisitos especificados pelo desafio Invillia, disponível [aqui](https://github.com/invillia/backend-challenge).

## Pré requisitos

Para executar o projeto, é necessário ter instalado:

 - Java 8
 - Maven (ou uma IDE que já venha com o plugin do Maven, como o Eclipse)

## Execução

Para executar o projeto, primeiramente, é necessário criar o artefato gerado pelo Maven, através do Eclipse ou através do comando (estando no diretório principal do projeto):

`mvn package`

Após gerar o artefato, este se encontrará na pasta *target*, gerada pelo Maven, com o nome *teste-0.0.1.jar*. Para subir a aplicação, basta executar o seguinte comando:

`java -jar target/stores-1.0-SNAPSHOT.jar`

A partir deste comando, caso seja executada localmente, a aplicação já estará funcionando [aqui](http://localhost:8000) e o console do banco H2 será mostrado [aqui](http://localhost:8000/h2-console).
Para utilizar o console para inspecionar os dados nas tabelas, tenha certeza de que esteja usando **jdbc:h2:mem:testdb** como a URL JDBC, no momento do login.

## Modelo

Os seguintes DTOs (*Data Transfer Object*) foram utilizados nesta API:

 - Order
 	```
 	{
		"confirmationDate": "2017-10-17 17:10:34",
		"address": {
			"country": "ARG",
			"state": "MG",
			"city": "JF",
			"street": "Rua A",
			"number": "75",
			"complement": "APE"
		},
		"items": [
			{
				"description": "Item 2-1",
				"unitPrice": 4.78,
				"quantity": 5
			},
			{
				"description": "Item 2-2",
				"unitPrice": 5.66,
				"quantity": 15
			}
		]
	}
 	```

 - OrderSearch
 	```
 	{
		"confirmationDate": "2017-10-17 17:10:34",
		"address": {
			"country": "ARG",
			"state": "MG",
			"city": "JF",
			"street": "Rua A",
			"number": "75",
			"complement": "APE"
		}
	}
 	```

 - Store
  	```
 	{
		"name": "Loja 3",
		"address": {
			"country": "BR",
			"state": "AC",
			"city": "JF",
			"street": "Rua A",
			"number": "75",
			"complement": "APE"
		}
	}
 	```

 - StoreSearch
  	```
 	{
		"name": "Loja 2",
		"address": {
			"country": "AG",
			"state": "MG",
			"city": "JF",
			"street": "Rua A",
			"number": "75",
			"complement": "APE"
		}
	}
 	```

 - Payment
  	```
 	{
		"creditCard": "4188933084507357",
		"paymentDate": "2018-02-17 17:10:34",
		"orderId": 1
	}
 	```

## Funcionalidades

Os funcionalidades da API podem ser verificadas através dos seguintes *urls* relativas:

 1. **Create a Store:** Requisição *POST* no *endpoint* **/stores**.
 2. **Update a Store information:** Requisição *PUT* no *endpoint* **/stores/{id}**. Caso a store não exista, será retornado um erro com *status* 404.
 3. **Retrieve a Store by parameters:** Requisição *POST* no *endpoint* **/stores/search**.
 4. **Retrieve an Store by id:** Requisição *GET* no *endpoint* **/stores/{id}**.
 5. **Create an Order with items:** Requisição *POST* no *endpoint* **/orders**. Esta requisição também adiciona ao banco os itens da ordem.
 6. **Create a Payment for an Order:** Requisição *POST* no *endpoint* **/payments**. Quando a ordem associada a este pagamento não existe, a API retorna um erro com *status* 404.
 7. **Retrieve an Order by parameters:** Requisição *POST* no *endpoint* **/orders/search**.
 8. **Retrieve an Order by id:** Requisição *GET* no *endpoint* **/orders/{id}**.
 9. **Refund Order or any Order Item:** Requisição *GET* no *endpoint* **/orders/refund/{id}**.

## Tecnologias

* [Java 8](https://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html) - Linguagem usada
* [Maven](https://maven.apache.org/) - Gerenciamento de dependências e *builds* e execução de testes
* [Spring Boot](https://spring.io/projects/spring-boot) - *Framework* utilizado
* [H2 Database](http://www.h2database.com/html/main.html) - Banco de dados em memória utilizado
* [Spring Data](http://projects.spring.io/spring-data/) - Utilitário para manusear objetos vindos do banco
