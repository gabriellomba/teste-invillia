package com.acme.stores.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.acme.stores.dto.PaymentDto;
import com.acme.stores.service.PaymentService;

@RestController
@RequestMapping("/payments")
public class PaymentRestController {

	@Autowired
	private PaymentService paymentService;

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public void createPayment(@Valid @RequestBody PaymentDto payment) {
		paymentService.create(payment);
	}
}
