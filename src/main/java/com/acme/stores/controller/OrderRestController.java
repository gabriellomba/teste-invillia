package com.acme.stores.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.acme.stores.dto.OrderDto;
import com.acme.stores.dto.OrderSearchDto;
import com.acme.stores.service.OrderService;

@RestController
@RequestMapping(value = "/orders")
public class OrderRestController {

	@Autowired
	private OrderService orderService;

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public void createOrder(@Valid @RequestBody OrderDto order) {
		orderService.create(order);
	}

	@RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public OrderDto getOrder(@PathVariable("id") Long orderId) {
		return orderService.getOrder(orderId);
	}

	@RequestMapping(value = "/refund/{id}")
	public void refundOrder(@PathVariable("id") Long orderId) {
		orderService.refund(orderId);
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<OrderDto> getOrders(@Valid @RequestBody(required = false) OrderSearchDto searchDto) {
		return orderService.getOrders(searchDto);
	}
}
