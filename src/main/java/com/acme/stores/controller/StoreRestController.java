package com.acme.stores.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.acme.stores.dto.StoreDto;
import com.acme.stores.dto.StoreSearchDto;
import com.acme.stores.service.StoreService;

@RestController
@RequestMapping("/stores")
public class StoreRestController {

	@Autowired
	private StoreService storeService;

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public void createStore(@Valid @RequestBody StoreDto store) {
		storeService.create(store);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void updateStore(@PathVariable("id") Long storeId, @Valid @RequestBody StoreDto store) {
		storeService.update(storeId, store);
	}
	
	@RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public StoreDto getOrder(@PathVariable("id") Long storeId) {
		return storeService.getStore(storeId);
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<StoreDto> getStores(@Valid @RequestBody(required = false) StoreSearchDto searchDto) {
		return storeService.getStores(searchDto);
	}
}
