package com.acme.stores.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.acme.stores.converter.PaymentStatusConverter;
import com.acme.stores.dto.PaymentDto;
import com.acme.stores.status.PaymentStatus;

@Entity
public class Payment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Convert(converter = PaymentStatusConverter.class)
	@Column(name = "STATUS", nullable = false)
	private PaymentStatus status;

	@Column(name = "CREDIT_CARD", nullable = false, length = 30)
	private String creditCard;

	@Column(name = "PAYMENT_DATE", nullable = false)
	private LocalDateTime paymentDate = LocalDateTime.now();

	@OneToOne(fetch = FetchType.LAZY)
	private Order order;

	public Payment() {
	}

	public Payment(PaymentDto dto) {
		this.status = dto.getStatus() == null ? PaymentStatus.COMPLETED : dto.getStatus();
		this.creditCard = dto.getCreditCard();
		this.paymentDate = dto.getPaymentDate();
		this.order = new Order(dto.getOrderId());
	}
	
	public LocalDateTime getPaymentDate() {
		return paymentDate;
	}

	public void setStatus(PaymentStatus status) {
		this.status = status;
	}
}
