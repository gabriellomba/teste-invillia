package com.acme.stores.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;

import com.acme.stores.converter.OrderStatusConverter;
import com.acme.stores.dto.OrderDto;
import com.acme.stores.dto.OrderItemDto;
import com.acme.stores.status.OrderStatus;

@Entity(name = "STORE_ORDER")
public class Order implements EntityWithDto<OrderDto> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Embedded
	private Address address;

	@Column(name = "CONFIRMATION_DATE", nullable = false)
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime confirmationDate;

	@Convert(converter = OrderStatusConverter.class)
	@Column(name = "STATUS", nullable = false)
	private OrderStatus status;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
	private List<OrderItem> items;

	public Order() {
	}

	public Order(Long id) {
		this.id = id;
	}

	public Order(OrderDto dto) {
		this.address = new Address(dto.getAddress());
		this.confirmationDate = dto.getConfirmationDate();
		this.status = dto.getStatus() == null ? OrderStatus.PENDING : dto.getStatus();
		this.items = dto.getItems().stream().map(OrderItem::new).collect(Collectors.toList());
	}

	@Override
	public OrderDto toDto() {
		List<OrderItemDto> itemsDto = this.items.stream().map(OrderItem::toDto).collect(Collectors.toList());

		return new OrderDto(this.address.toDto(), this.confirmationDate, this.status, itemsDto);
	}

	public LocalDateTime getConfirmationDate() {
		return confirmationDate;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public List<OrderItem> getItems() {
		return items;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}
}
