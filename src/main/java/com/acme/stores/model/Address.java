package com.acme.stores.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.acme.stores.dto.AddressDto;

@Embeddable
public class Address implements EntityWithDto<AddressDto> {

	@Column(name = "COUNTRY", nullable = false, length = 75)
	private String country;

	@Column(name = "STATE", nullable = false, length = 50)
	private String state;

	@Column(name = "CITY", nullable = false, length = 100)
	private String city;

	@Column(name = "STREET", nullable = false, length = 200)
	private String street;

	@Column(name = "STREET_NUMBER", nullable = false, length = 30)
	private String number;

	@Column(name = "COMPLEMENT", length = 50)
	private String complement;

	public Address() {
	}

	public Address(AddressDto dto) {
		this.country = dto.getCountry();
		this.state = dto.getState();
		this.city = dto.getCity();
		this.street = dto.getStreet();
		this.number = dto.getNumber();
		this.complement = dto.getComplement();
	}

	@Override
	public AddressDto toDto() {
		return new AddressDto(country, state, city, street, number, complement);
	}

}