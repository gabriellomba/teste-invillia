package com.acme.stores.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.acme.stores.dto.StoreDto;

@Entity
public class Store implements EntityWithDto<StoreDto> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "NAME", nullable = false, length = 100)
	private String name;

	@Embedded
	private Address address;
	
	public Store(){
	}
	
	public Store(StoreDto dto) {
		this.name = dto.getName();
		this.address = new Address(dto.getAddress());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long storeId) {
		this.id = storeId;
	}

	@Override
	public StoreDto toDto() {
		return new StoreDto(this.name, this.address.toDto());
	}
}
