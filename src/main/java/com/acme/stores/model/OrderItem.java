package com.acme.stores.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.acme.stores.dto.OrderItemDto;

@Entity
public class OrderItem implements EntityWithDto<OrderItemDto> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "DESCRIPTION", nullable = false, length = 300)
	private String description;

	@Column(name = "UNIT_PRICE", nullable = false)
	private BigDecimal unitPrice;

	@Column(name = "QUANTITY", nullable = false)
	private Long quantity;

	@ManyToOne(fetch = FetchType.LAZY)
	private Order order;

	public OrderItem() {
	}

	public OrderItem(OrderItemDto dto) {
		this.description = dto.getDescription();
		this.unitPrice = dto.getUnitPrice();
		this.quantity = dto.getQuantity();
	}

	@Override
	public OrderItemDto toDto() {
		return new OrderItemDto(this.description, this.unitPrice, this.quantity);
	}

}
