package com.acme.stores.model;

public interface EntityWithDto<T> {

	T toDto();

}
