package com.acme.stores.exception;

import org.springframework.http.HttpStatus;

public final class OrderNotPaidException extends AbstractHttpException {

	private static final long serialVersionUID = 4083832976308045965L;
	private static final String ERROR_MESSAGE = "Esta ordem ainda não foi paga.";

	public OrderNotPaidException() {
		super(HttpStatus.UNPROCESSABLE_ENTITY, ERROR_MESSAGE);
	}
}
