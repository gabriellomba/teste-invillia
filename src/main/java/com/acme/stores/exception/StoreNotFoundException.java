package com.acme.stores.exception;

import org.springframework.http.HttpStatus;

public final class StoreNotFoundException extends AbstractHttpException {

	private static final long serialVersionUID = 4083832976308045965L;
	private static final String ERROR_MESSAGE = "Não existe uma loja com o id fornecido.";

	public StoreNotFoundException() {
		super(HttpStatus.NOT_FOUND, ERROR_MESSAGE);
	}
}
