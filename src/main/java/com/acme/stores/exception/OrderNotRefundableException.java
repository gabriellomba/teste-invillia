package com.acme.stores.exception;

import org.springframework.http.HttpStatus;

public final class OrderNotRefundableException extends AbstractHttpException {

	private static final long serialVersionUID = 4083832976308045965L;
	private static final String ERROR_MESSAGE = "O prazo de estorno para esta ordem se esgotou.";

	public OrderNotRefundableException() {
		super(HttpStatus.UNPROCESSABLE_ENTITY, ERROR_MESSAGE);
	}
}
