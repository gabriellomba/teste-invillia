package com.acme.stores.exception;

import org.springframework.http.HttpStatus;

public final class OrderAlreadyRefundedException extends AbstractHttpException {

	private static final long serialVersionUID = 4083832976308045965L;
	private static final String ERROR_MESSAGE = "Esta ordem já foi estornada.";

	public OrderAlreadyRefundedException() {
		super(HttpStatus.UNPROCESSABLE_ENTITY, ERROR_MESSAGE);
	}
}
