package com.acme.stores.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.acme.stores.model.Address;
import com.acme.stores.model.Order;
import com.acme.stores.status.OrderStatus;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

	@Query("FROM STORE_ORDER o JOIN FETCH o.items")
	List<Order> findAll();

	@Query("FROM STORE_ORDER o JOIN FETCH o.items WHERE (?1 IS NULL OR o.confirmationDate = ?1) AND (?2 IS NULL OR o.status = ?2)")
	List<Order> findByConfirmationDateAndStatus(LocalDateTime confirmationDate, OrderStatus status);

	List<Order> findByAddressAndConfirmationDate(Address address, LocalDateTime confirmationDate);

	List<Order> findByAddressAndStatus(Address address, OrderStatus status);

	List<Order> findByAddressAndConfirmationDateAndStatus(Address address, LocalDateTime confirmationDate,
			OrderStatus status);
}