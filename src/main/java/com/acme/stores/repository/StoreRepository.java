package com.acme.stores.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.acme.stores.model.Address;
import com.acme.stores.model.Store;

@Repository
public interface StoreRepository extends CrudRepository<Store, Long> {

	List<Store> findByName(String name);

	List<Store> findByAddress(Address address);

	List<Store> findByNameAndAddress(String name, Address address);

}
