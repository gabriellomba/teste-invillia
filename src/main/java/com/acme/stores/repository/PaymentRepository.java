package com.acme.stores.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.acme.stores.model.Payment;

@Repository
public interface PaymentRepository extends CrudRepository<Payment, Long> {

	public Payment findOneByOrderId(Long orderId);

}