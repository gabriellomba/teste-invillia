package com.acme.stores.handler;

import java.util.Date;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.acme.stores.exception.AbstractHttpException;

@ControllerAdvice
public class AbstractHttpExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(AbstractHttpException.class)
	public final ResponseEntity<ErrorDetails> handleUserNotFoundException(AbstractHttpException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, ex.getStatus());
	}
}
