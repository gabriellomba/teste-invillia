package com.acme.stores.status;

public enum OrderStatus implements Status {
	PENDING("PE"), PAID("PA"), REFUNDED("RE");

	private String dbRepresentation;

	private OrderStatus(String dbRepresentation) {
		this.dbRepresentation = dbRepresentation;
	}

	@Override
	public String getDbRepresentation() {
		return dbRepresentation;
	}

	public static OrderStatus getEnumRepresentation(String dbRepresentation) {
		return Status.getEnumRepresentation(dbRepresentation, values());
	}
}
