package com.acme.stores.status;

public interface Status {

	public String getDbRepresentation();

	public static <T extends Status> T getEnumRepresentation(String dbRepresentation, T[] possibleStatus) {
		for (T status : possibleStatus) {
			if (status.getDbRepresentation().equals(dbRepresentation)) {
				return status;
			}
		}

		throw new IllegalArgumentException(
				String.format("Representação de status não reconhecida: %s. Escolas possíveis: %s .",
						dbRepresentation, possibleStatus));
	}
}
