package com.acme.stores.status;

public enum PaymentStatus implements Status {
	COMPLETED("CO"), REFUNDED("RE");

	private String dbRepresentation;

	private PaymentStatus(String dbRepresentation) {
		this.dbRepresentation = dbRepresentation;
	}

	@Override
	public String getDbRepresentation() {
		return dbRepresentation;
	}

	public static PaymentStatus getEnumRepresentation(String dbRepresentation) {
		return Status.getEnumRepresentation(dbRepresentation, values());
	}
}
