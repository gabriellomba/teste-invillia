package com.acme.stores.converter;

import java.util.Optional;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.acme.stores.status.OrderStatus;

@Converter(autoApply = true)
public class OrderStatusConverter implements AttributeConverter<OrderStatus, String> {

	@Override
	public String convertToDatabaseColumn(OrderStatus status) {
		return Optional.ofNullable(status).orElse(OrderStatus.PENDING).getDbRepresentation();
	}

	@Override
	public OrderStatus convertToEntityAttribute(String dbRepresentation) {
		return OrderStatus.getEnumRepresentation(dbRepresentation);
	}
}
