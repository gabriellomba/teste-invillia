package com.acme.stores.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.acme.stores.status.PaymentStatus;

@Converter(autoApply = true)
public class PaymentStatusConverter implements AttributeConverter<PaymentStatus, String> {

	@Override
	public String convertToDatabaseColumn(PaymentStatus status) {
		return status.getDbRepresentation();
	}

	@Override
	public PaymentStatus convertToEntityAttribute(String dbRepresentation) {
		return PaymentStatus.getEnumRepresentation(dbRepresentation);
	}
}
