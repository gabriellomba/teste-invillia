package com.acme.stores.service;

import java.util.List;

import com.acme.stores.dto.StoreDto;
import com.acme.stores.dto.StoreSearchDto;

public interface StoreService {

	public void create(StoreDto store);

	public void update(Long storeId, StoreDto store);
	
	public StoreDto getStore(Long storeId);

	public List<StoreDto> getStores(StoreSearchDto searchDto);

}
