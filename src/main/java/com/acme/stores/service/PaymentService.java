package com.acme.stores.service;

import com.acme.stores.dto.PaymentDto;

public interface PaymentService {

	public void create(PaymentDto payment);
}
