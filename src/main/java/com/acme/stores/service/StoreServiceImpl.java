package com.acme.stores.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acme.stores.dto.StoreDto;
import com.acme.stores.dto.StoreSearchDto;
import com.acme.stores.exception.StoreNotFoundException;
import com.acme.stores.model.Address;
import com.acme.stores.model.Store;
import com.acme.stores.repository.StoreRepository;

@Service
public class StoreServiceImpl implements StoreService {

	@Autowired
	private StoreRepository storeRepository;

	@Override
	@Transactional
	public void create(StoreDto dto) {
		storeRepository.save(new Store(dto));
	}

	@Override
	@Transactional
	public void update(Long storeId, StoreDto dto) {
		if (!storeRepository.exists(storeId)) {
			throw new StoreNotFoundException();
		}

		Store store = new Store(dto);
		store.setId(storeId);

		storeRepository.save(store);
	}

	@Override
	public StoreDto getStore(Long storeId) {
		Store store = storeRepository.findOne(storeId);

		if (store == null) {
			throw new StoreNotFoundException();
		}

		return store.toDto();
	}

	@Override
	public List<StoreDto> getStores(StoreSearchDto searchDto) {
		List<Store> stores;

		boolean hasName = searchDto != null && searchDto.getName() != null;
		boolean hasAddress = searchDto != null && searchDto.getAddress() != null;

		if (hasName && hasAddress) {
			stores = storeRepository.findByNameAndAddress(searchDto.getName(), new Address(searchDto.getAddress()));
		} else if (hasName) {
			stores = storeRepository.findByName(searchDto.getName());
		} else if (hasAddress) {
			stores = storeRepository.findByAddress(new Address(searchDto.getAddress()));
		} else {
			stores = (List<Store>) storeRepository.findAll();
		}

		return stores.stream().map(Store::toDto).collect(Collectors.toList());
	}
}
