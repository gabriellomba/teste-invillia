package com.acme.stores.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acme.stores.dto.PaymentDto;
import com.acme.stores.exception.OrderNotFoundException;
import com.acme.stores.model.Order;
import com.acme.stores.model.Payment;
import com.acme.stores.repository.OrderRepository;
import com.acme.stores.repository.PaymentRepository;
import com.acme.stores.status.OrderStatus;

@Service
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	private PaymentRepository paymentRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Override
	@Transactional
	public void create(PaymentDto paymentDto) {
		Order order = orderRepository.findOne(paymentDto.getOrderId());

		if (order == null) {
			throw new OrderNotFoundException();
		}

		Payment payment = new Payment(paymentDto);
		paymentRepository.save(payment);
		
		order.setStatus(OrderStatus.PAID);
		orderRepository.save(order);
	}
}
