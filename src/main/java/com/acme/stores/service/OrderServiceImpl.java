package com.acme.stores.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acme.stores.dto.OrderDto;
import com.acme.stores.dto.OrderSearchDto;
import com.acme.stores.exception.OrderAlreadyRefundedException;
import com.acme.stores.exception.OrderNotFoundException;
import com.acme.stores.exception.OrderNotPaidException;
import com.acme.stores.exception.OrderNotRefundableException;
import com.acme.stores.model.Address;
import com.acme.stores.model.Order;
import com.acme.stores.model.OrderItem;
import com.acme.stores.model.Payment;
import com.acme.stores.repository.OrderItemRepository;
import com.acme.stores.repository.OrderRepository;
import com.acme.stores.repository.PaymentRepository;
import com.acme.stores.status.OrderStatus;
import com.acme.stores.status.PaymentStatus;

@Service
public class OrderServiceImpl implements OrderService {

	private static final Integer MAX_DAYS_PASSED_TO_REFUND = 10;

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderItemRepository orderItemRepository;

	@Autowired
	private PaymentRepository paymentRepository;

	@Override
	@Transactional
	public void create(OrderDto orderDto) {
		Order order = new Order(orderDto);

		for (OrderItem item : order.getItems()) {
			orderItemRepository.save(item);
		}

		orderRepository.save(order);
	}

	@Override
	public OrderDto getOrder(Long orderId) {
		Order order = orderRepository.findOne(orderId);

		if (order == null) {
			throw new OrderNotFoundException();
		}

		return order.toDto();
	}

	@Override
	public List<OrderDto> getOrders(OrderSearchDto searchDto) {
		List<Order> orders;

		boolean hasConfirmationDate = searchDto != null && searchDto.getConfirmationDate() != null;
		boolean hasStatus = searchDto != null && searchDto.getStatus() != null;
		boolean hasAddress = searchDto != null && searchDto.getAddress() != null;

		if (hasConfirmationDate && hasStatus && hasAddress) {
			orders = orderRepository.findByAddressAndConfirmationDateAndStatus(new Address(searchDto.getAddress()),
					searchDto.getConfirmationDate(), searchDto.getStatus());
		} else if (hasConfirmationDate && hasAddress) {
			orders = orderRepository.findByAddressAndConfirmationDate(new Address(searchDto.getAddress()),
					searchDto.getConfirmationDate());
		} else if (hasStatus && hasAddress) {
			orders = orderRepository.findByAddressAndStatus(new Address(searchDto.getAddress()), searchDto.getStatus());
		} else if (hasConfirmationDate || hasStatus) {
			orders = orderRepository.findByConfirmationDateAndStatus(searchDto.getConfirmationDate(),
					searchDto.getStatus());
		} else {
			orders = orderRepository.findAll();
		}

		return orders.stream().map(Order::toDto).collect(Collectors.toList());
	}

	@Override
	public void refund(Long orderId) {
		Order order = orderRepository.findOne(orderId);

		if (order == null) {
			throw new OrderNotFoundException();
		}
		
		if (order.getStatus().equals(OrderStatus.REFUNDED)) {
			throw new OrderAlreadyRefundedException();
		}

		if (!order.getStatus().equals(OrderStatus.PAID)) {
			throw new OrderNotPaidException();
		}

		Payment payment = paymentRepository.findOneByOrderId(orderId);
		
		LocalDate orderUpdateDate = payment.getPaymentDate().isAfter(order.getConfirmationDate()) ?
				payment.getPaymentDate().toLocalDate() : order.getConfirmationDate().toLocalDate();
				
		if (Math.abs(ChronoUnit.DAYS.between(LocalDate.now(), orderUpdateDate)) > MAX_DAYS_PASSED_TO_REFUND) {
			throw new OrderNotRefundableException();
		}
		
		order.setStatus(OrderStatus.REFUNDED);
		orderRepository.save(order);
		
		payment.setStatus(PaymentStatus.REFUNDED);
		paymentRepository.save(payment);
	}
}
