package com.acme.stores.service;

import java.util.List;

import com.acme.stores.dto.OrderDto;
import com.acme.stores.dto.OrderSearchDto;

public interface OrderService {

	public void create(OrderDto order);
	
	public OrderDto getOrder(Long orderId);

	public List<OrderDto> getOrders(OrderSearchDto searchDto);

	public void refund(Long orderId);
}
