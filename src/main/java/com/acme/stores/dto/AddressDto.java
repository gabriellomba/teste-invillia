package com.acme.stores.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AddressDto implements Serializable {

	private static final long serialVersionUID = -5801893886051191405L;

	@NotNull(message = "O país do endereço é um valor obrigatório.")
	@Size(min = 1, max = 75, message = "O país do endereço deve ter entre {min} e {max} caracteres.")
	private String country;

	@NotNull(message = "O estado do endereço é um campo obrigatório.")
	@Size(min = 1, max = 50, message = "O estado do endereço deve ter entre {min} e {max} caracteres.")
	private String state;

	@NotNull(message = "A cidade do endereço é um valor obrigatório.")
	@Size(min = 1, max = 100, message = "A cidade do endereço deve ter entre {min} e {max} caracteres.")
	private String city;

	@NotNull(message = "A rua do endereço é um valor obrigatório.")
	@Size(min = 1, max = 200, message = "A rua do endereço deve ter entre {min} e {max} caracteres.")
	private String street;

	@NotNull(message = "O número do endereço é um valor obrigatório.")
	@Size(min = 1, max = 30, message = "O número do endereço deve ter entre {min} e {max} caracteres.")
	private String number;

	@Size(max = 50, message = "O complemento do endereço deve ter entre {min} e {max} caracteres.")
	private String complement;

	@JsonCreator
	public AddressDto(@JsonProperty("country") String country, @JsonProperty("state") String state,
			@JsonProperty("city") String city, @JsonProperty("street") String street,
			@JsonProperty("number") String number, @JsonProperty("complement") String complement) {
		this.country = country;
		this.state = state;
		this.city = city;
		this.street = street;
		this.number = number;
		this.complement = complement;
	}

	public String getCountry() {
		return country;
	}

	public String getState() {
		return state;
	}

	public String getCity() {
		return city;
	}

	public String getStreet() {
		return street;
	}

	public String getNumber() {
		return number;
	}

	public String getComplement() {
		return complement;
	}
}
