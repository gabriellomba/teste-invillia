package com.acme.stores.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.acme.stores.serializer.LocalDateTimeDeserializer;
import com.acme.stores.serializer.LocalDateTimeSerializer;
import com.acme.stores.status.OrderStatus;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class OrderDto implements Serializable {

	private static final long serialVersionUID = -1678597394566609705L;

	@NotNull(message = "O endereço da ordem é um campo obrigatório.")
	private AddressDto address;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@NotNull(message = "A data de confirmação da ordem é um campo obrigatório.")
	private LocalDateTime confirmationDate;

	private OrderStatus status;

	@NotNull(message = "Os itens de uma ordem são um campo obrigatório.")
	@Size(min = 1, message = "Uma ordem deve conter, ao menos, um item.")
	private List<OrderItemDto> items;

	@JsonCreator
	public OrderDto(@JsonProperty("address") AddressDto address, @JsonProperty("confirmationDate") LocalDateTime confirmationDate,
			@JsonProperty("status") OrderStatus status, @JsonProperty("items") List<OrderItemDto> items) {
		this.address = address;
		this.confirmationDate = confirmationDate;
		this.status = status;
		this.items = items;
	}

	public AddressDto getAddress() {
		return address;
	}

	public LocalDateTime getConfirmationDate() {
		return confirmationDate;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public List<OrderItemDto> getItems() {
		return items;
	}
}
