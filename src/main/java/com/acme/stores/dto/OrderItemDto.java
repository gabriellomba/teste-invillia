package com.acme.stores.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderItemDto implements Serializable {

	private static final long serialVersionUID = -1079005872222632119L;

	@NotNull(message = "A descrição do ítem é um campo obrigatório.")
	@Size(min = 1, max = 300, message = "A descrição do ítem deve ter entre {min} e {max} caracteres")
	private String description;

	@NotNull(message = "O preço unitário do ítem é um campo obrigatório.")
	private BigDecimal unitPrice;

	@NotNull(message = "A quantidade de itens é um campo obrigatório.")
	private Long quantity;
	
	@JsonCreator
	public OrderItemDto(@JsonProperty("description") String description, @JsonProperty("unitPrice") BigDecimal unitPrice,
			@JsonProperty("quantity") Long quantity) {
		this.description = description;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public Long getQuantity() {
		return quantity;
	}

}
