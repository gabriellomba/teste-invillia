package com.acme.stores.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StoreDto implements Serializable {

	private static final long serialVersionUID = -2419938124264412751L;

	@NotNull(message = "O nome da loja é um campo obrigatório.")
	@Size(min = 1, max = 100, message = "O nome da loja deve ter entre {min} e {max} caracteres")
	private String name;

	@NotNull(message = "O endereço da ordem é um campo obrigatório.")
	private AddressDto address;

	@JsonCreator
	public StoreDto(@JsonProperty("name") String name, @JsonProperty("address") AddressDto address) {
		this.name = name;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public AddressDto getAddress() {
		return address;
	}

}
