package com.acme.stores.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.acme.stores.serializer.LocalDateTimeDeserializer;
import com.acme.stores.serializer.LocalDateTimeSerializer;
import com.acme.stores.status.OrderStatus;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class OrderSearchDto implements Serializable {

	private static final long serialVersionUID = -2931475245522109266L;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime confirmationDate;

	private OrderStatus status;

	private AddressDto address;

	public LocalDateTime getConfirmationDate() {
		return confirmationDate;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public AddressDto getAddress() {
		return address;
	}
}
