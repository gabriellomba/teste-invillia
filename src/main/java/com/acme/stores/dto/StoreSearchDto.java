package com.acme.stores.dto;

import java.io.Serializable;

public class StoreSearchDto implements Serializable {

	private static final long serialVersionUID = -3450067755157858386L;

	private String name;

	private AddressDto address;

	public String getName() {
		return name;
	}

	public AddressDto getAddress() {
		return address;
	}
}
