package com.acme.stores.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.CreditCardNumber;

import com.acme.stores.serializer.LocalDateTimeDeserializer;
import com.acme.stores.serializer.LocalDateTimeSerializer;
import com.acme.stores.status.PaymentStatus;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class PaymentDto implements Serializable {

	private static final long serialVersionUID = 5974576873067067429L;

	private PaymentStatus status;

	@NotNull(message = "O cartão de crédito do pagamento é um campo obrigatório.")
	@Size(min = 1, max = 30, message = "O cartão de crédito do pagamento deve ter entre {min} e {max} caracteres")
	@CreditCardNumber(message = "O cartão de crédito é inválido")
	private String creditCard;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime paymentDate = LocalDateTime.now();

	private Long orderId;

	public PaymentStatus getStatus() {
		return status;
	}

	public String getCreditCard() {
		return creditCard;
	}

	public LocalDateTime getPaymentDate() {
		return paymentDate;
	}

	public Long getOrderId() {
		return orderId;
	}
}
